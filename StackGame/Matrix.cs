﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace StackGame
{
    class Matrix
    {
        // Lista que almacena la matriz.
        // Solo un space debe hacer uso de esta propiedad.
        public List<Space> spaces;

        // Recuento de cuantas filas x columnas caben en la forma actual
        public int filas, columnas;

        // Valores por defecto de la pluma.
        // Modificar aquí para alterar el tamaño de los cuadros y el espaciado.
        public static int DefaultSize = 10;
        public static int DefaultSpacing = 11;

        public Form form;

        /// <summary>
        /// Indizador de la lista de espacios.
        /// Regresa el Space buscado
        /// </summary>
        /// <param name="fil">Fila buscada</param>
        /// <param name="col">Columna buscada</param>
        /// <returns></returns>
        public Space this[int fil, int col]
        {
            get
            {
                try
                {
                    return spaces[(columnas * fil) + col];
                }
                catch (System.Exception)
                {
                    return null;
                }
            }
        }

        public Matrix(Form f)
        {
            filas = columnas = 0;
            spaces = new List<Space>();
            form = f;
        }

        /// <summary>
        /// Crea la matriz de espacios.
        /// Determina las posiciones x,y de cada espacio.
        /// Agrega los espacios a la lista.
        /// </summary>
        /// <param name="f">Forma sobre la que se trabaja</param>
        public void CreateMatrix()
        {
            int x = 10;
            int y = 10;
            int fila = 0;
            int columna = 0;

            while (x <= form.Width && y <= form.Height)
            {
                spaces.Add(new Space(fila, columna, x, y));
                if (x + DefaultSpacing + DefaultSpacing + DefaultSize + 22 < form.Width)
                {
                    x += DefaultSpacing + DefaultSize;
                    columna++;
                    if (columnas < columna)
                        columnas = columna;
                }
                else if (y + DefaultSpacing + DefaultSpacing + DefaultSize + 40 < form.Height)
                {
                    x = 10;
                    y += DefaultSpacing + DefaultSize;
                    columna = 0;
                    fila++;
                    if (filas < fila)
                        filas = fila;
                }
                else
                {
                    break;
                }
            }

            filas++;
            columnas++;
        }

        public bool MoveDown(Space space)
        {
            Space pivote = this[space.fila + 1, space.columna];
            if (pivote != null && !pivote.used)
            {
                space.ToggleUsed();
                this[space.fila + 1, space.columna].ToggleUsed();
                return true;
            }
            return false;
        }

        public bool MoveLR(Space space, char direction)
        {
            int izq_der;
            Space pivote = null;
            switch (direction)
            {
                case 'L':
                    izq_der = -1;
                        break;
                case 'R':
                    izq_der = 1;
                        break;
                default:
                    return false;
            }

            if ((space.columna + izq_der < columnas && space.columna + izq_der >= 0))
                pivote = this[space.fila, space.columna + izq_der];

            if (pivote != null && !pivote.used)
            {
                space.ToggleUsed();
                this[space.fila, space.columna + izq_der].ToggleUsed();
                return true;
            }
            return false;
        }
    }
}
