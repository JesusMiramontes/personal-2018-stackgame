﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StackGame
{
    public partial class Form1 : Form
    {
        Matrix matriz;
        private int i = 10;
        public static Drawing d;
        bool keep_running = true;


        public Form1()
        {
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            matriz = new Matrix(this);
            d = new Drawing(matriz.form);
            matriz.CreateMatrix();
            //matriz[10,0].ToggleUsed();
            matriz[10,10].ToggleUsed();
            timer1.Enabled = true;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (keep_running && i >= 0)
            {
                if (!matriz.MoveLR(matriz[10, i],'L'))
                    keep_running = false;
                i--;
                Thread.Sleep(20);
            }
        }
    }
}
