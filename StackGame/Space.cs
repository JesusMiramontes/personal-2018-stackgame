﻿using System.Drawing;

namespace StackGame
{
    public class Space
    {
        /*
         * La numeración comienza de la esquina superior izquierda de la pantalla
         * Se incrementa la columna hasta llegar al fin de la forma
         * Despues se incrementa la fila y se establece la columna a 0
         */

        // Ubicación del espacio <fila,columna>
        public int fila { get; set; }
        public int columna { get; set; }

        // ¿La forma está en uso?
        public bool used { get; set; }

        // Rectangulo que se usa para dibujarse
        public Rectangle r { get; set; }

        public Space()
        {
            used = false;
            r = new Rectangle();
        }

        public Space(int fila, int columna, int x, int y)
        {
            used = false;
            this.fila = fila;
            this.columna = columna;
            r = new Rectangle(x,y,Matrix.DefaultSize, Matrix.DefaultSize);
        }

        /// <summary>
        /// Si el espacio está en uso lo marca como disponible y lo borra de pantalla
        /// Visceversa
        /// </summary>
        public void ToggleUsed()
        {
            if (!used)
                { Form1.d.dibujar(this);}
            else
                { Form1.d.desdibujar(this);}
            used = !used;
        }

       
    }
}
